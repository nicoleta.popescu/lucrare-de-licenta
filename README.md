#Lucrare de licență: Joc dezvoltat în Construct 3

Link proiect gitlab: https://gitlab.upt.ro/nicoleta.popescu/lucrare-de-licenta.git
Primul pas: Descărcarea fișierului Licenta_PNA.c3p din gitlab -> Lucrare de licenta -> Construct

Link Construct 3: https://editor.construct.net/?startTour
După deschiderea link-ului spre Construct apare o fereastră "Welcome to Construct!". Alegeți opțiunea: "No thanks, not now"

În pagina web deschisă, pașii de lansare a aplicației sunt următorii:
-Open -> File
-În fereastra deschisă, se alege fișierul Licenta_PNA.c3p descărcat pe local și apoi Open
-După ce proiectul s-a deschis, se selectează "Home Page" din bara de instrumente de sus a platformei
-Tot din bara de instrumente din partea de sus se selectează "Play"/"Preview"

Instrucțiuni:
-Săgețile de pe tastatură sunt folosite pentru mișcarea caracterului
-Tasta Space este folosită pentru atac
-Inamicii mici sunt eliminați prin săritură pe capul lor
-Inamicul principal are 3 vieți, iar la fiecare lovitură pierde câte una
-Cufărul trebuie atins pentru a ajunge la partea de puzzle, unde piesele trebuie aranjate ca în imaginea exemplu
-Pentru a trece la nivelul următor, mașina timpului trebuie apăsată
-La celelalte 2 nivele, pașii se repetă
